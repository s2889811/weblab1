package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {


    private Map<String,Double> map;

    public  Quoter(){
        map = new HashMap<>();
        map.put("1",10.);
        map.put("2",45.);
        map.put("3",20.);
        map.put("4",35.);
        map.put("5",50.);
        map.put("others",0.);
    }
    public double getBookPrice(String isbn) {
        return map.get(isbn);
    }
}
